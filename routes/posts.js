var express = require("express");
const { randomBytes } = require("crypto");
var router = express.Router();

const posts = {};
const commentsByPostId = {};

/* GET posts listing. */
router.get("/", (req, res) => {
  res.render("index", { title: "Express", posts: posts });
});
router.post("/create", async (req, res) => {
  const id = randomBytes(4).toString("hex");
  // k5lk3j4j3fgfg
  const { title } = req.body;
  console.log(title);
  // Guardando data
  posts[id] = { id, title };
  console.log("Evento PostCreated emitido", posts);
  // Devolver al cliente
  res.redirect(`/`);
});
router.post("/:id/comments", async (req, res) => {
  const commentId = randomBytes(4).toString("hex");
  // k5lk3j4j3fgfg
  const { content } = req.body;
  const comments = commentsByPostId[req.params.id] || [];
  comments.push({ id: commentId, content, status: "pending" });
  commentsByPostId[req.params.id] = comments;

  console.log("Evento CommentCreated emitido", comments);
  console.log(comments);
  // Asignar el comentario a un post
  posts[req.params.id].comments = comments;

  console.log(posts);
  // Devolver al cliente
  res.redirect(`/`);
});

router.post("/:id/comments/:idc", async (req, res) => {
  const { id, idc } = req.params;
  const comment = commentsByPostId[id].find((c) => c.id === idc);
  const status = comment.content.includes("naranja") ? "rejected" : "approved";
  comment.status = status;
  console.log("Evento CommentUpdated emitido", comment);
  // actualiza el comentario
  posts[id].comments = { ...commentsByPostId[id] };

  res.redirect(`/`);
});

router.post("/audit", async (req, res) => {
  const comments = Object.values(commentsByPostId).flat();
  comments.forEach((comment) => {
    if (comment.status === "pending") {
      comment.status = comment.content.includes("naranja")
        ? "rejected"
        : "approved";
    }
  });
  res.status(200).json({
    ok: true,
    message: "Audit completed",
  });
});

module.exports = router;
